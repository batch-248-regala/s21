let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

   // Function to add a user
   function addUser(user) {
     users[users.length]=user;
   }

   // Invoking the function and adding an argument
   addUser("John Cena");

   // Logging the updated users array in the console
   console.log("Updated Array:");
   console.log(users);


/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

    // Function to return a user based on index
    function getUser(index) {
      return users[index];
    }

    // Invoking the function and storing the returned value in a variable
    let userFound = getUser(2);

    // Logging the itemFound variable in the console
    console.log("User Found:", userFound);

/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

    // Function to delete the last item in the users array
    function deleteLastUser() {
      let lastUser = users[users.length - 1];
      users.length -= 1;
      return lastUser;
    }

    let deletedUser = deleteLastUser();

    console.log("Deleted User:", deletedUser);


/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/
    console.log("Original Array:")
    console.log(users);
    // Function to update a user in the users array
    function updateUser(update, index) {
    users[index] = update;
    }

    // Invoking the function and updating a user in the users array
    updateUser("Triple H", 1);

    // Logging the updated users array in the console
    console.log("New Array:");
    console.log(users);


/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

    // Function to delete all items in the users array
    function deleteAllItems() {
    users.length = 0;
    }

    deleteAllItems();

    console.log("Array after deleting all items:", users);



/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
    // Function to check if the users array is empty
    function checkIfUsersArrayIsEmpty() {
    if (users.length > 0) {
    return false;
    } else {
    return true;
    }
    }

    // Storing the returned value from the function in a variable
    let isUsersEmpty = checkIfUsersArrayIsEmpty();

    // Logging the isUsersEmpty variable in the console
    console.log("Is users[] empty?", isUsersEmpty);

